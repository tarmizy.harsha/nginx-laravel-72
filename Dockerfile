FROM tarmizy/laravel-nginx-php72

ENV LC_ALL=C.UTF-8
WORKDIR /var/www/html

COPY nginx.conf /etc/nginx/nginx.conf
COPY php-fpm.conf /etc/php/7.2/fpm/php-fpm.conf
COPY defaults.ini /etc/php/7.2/cli/conf.d/defaults.ini
COPY defaults.ini /etc/php/7.2/fpm/conf.d/defaults.ini
ADD . .
RUN mkdir -p /run/php && touch /run/php/php7.2-fpm.sock && touch /run/php/php7.2-fpm.pid

COPY entrypoint.sh /entrypoint.sh
RUN chmod 755 /entrypoint.sh
RUN chown -R www-data: /var/www/html
RUN chmod -R 755 /var/www/html/storage

EXPOSE 80

CMD ["/entrypoint.sh"]
